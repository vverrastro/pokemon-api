﻿namespace PokemonApi.Common {
    /// <summary>
    /// Constants used for clients configuration
    /// </summary>
    public static class ClientType {
        public const string PokeApi = "PokeApi";
        public const string Shakespeare = "Shakespeare";
    }
}
