﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.VisualBasic;
using PokemonApi.Models;
using PokemonApi.Services.Interfaces;

namespace PokemonApi.Controllers {

    /// <summary>
    /// PokemonApi controller
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class PokemonController : ControllerBase {

        private readonly ILogger<PokemonController> _logger;
        private readonly IPokemonService _pokemonService;

        public PokemonController(ILogger<PokemonController> logger, IPokemonService pokemonService) {
            _logger = logger;
            _pokemonService = pokemonService;
        }

        /// <summary>
        /// The method returns the name and the Shakespearean description of the pokemon
        /// </summary>
        /// <param name="name">The input is the pokemon name and description translated by Shakespeare service</param>
        /// <returns>Returns the pokemon name </returns>
        /// <response code="200">Returns the pokemon data with name and description translated by Shakespeare service</response>
        /// <response code="400">Invalid input</response>
        /// <response code="404">Pokemon not found</response> 
        [HttpGet("{name}")]
        [ProducesResponseType(typeof(ServiceResponse<Pokemon>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ServiceResponse<Pokemon>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ServiceResponse<Pokemon>), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetPokemon(string name) {
            _logger.LogDebug($"{DateTime.UtcNow.ToLongTimeString()} - PokemonController - GetPokemon: {name}");
            ServiceResponse<Pokemon> response = new ServiceResponse<Pokemon>();

            if (Information.IsNumeric(name)) {
                response.Success = false;
                response.Message = "Please use a correct pokemon name!";
                return BadRequest(response);
            }

            response = await _pokemonService.GetPokemonFromPokeApi(name);

            if (response.Data == null) {
                response.Success = false;
                response.Message = "Pokemon or description not found!";
                return NotFound(response);
            }
            
            response = await _pokemonService.GetShakespeareanDescription(response.Data);
            return Ok(response);
        }

    }
}
