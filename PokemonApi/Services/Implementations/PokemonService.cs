﻿using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Extensions.Logging;
using PokemonApi.Common;
using PokemonApi.Models;
using PokemonApi.Models.JsonStructures;
using PokemonApi.Services.Interfaces;

namespace PokemonApi.Services.Implementations {

    /// <summary>
    /// This service is used to concat pokeapi and shakespeare translation APIs
    /// </summary>
    public class PokemonService : IPokemonService {

        private readonly ILogger<PokemonService> _logger;
        private readonly IHttpClientFactory _clientFactory;

        public PokemonService(ILogger<PokemonService> logger, IHttpClientFactory clientFactory) {
            _logger = logger;
            _clientFactory = clientFactory;
        }

        /// <summary>
        /// This method calls the pokeapi endpoint in order to create the pokemon object with name and description
        /// </summary>
        /// <param name="name">It's the pokemon name</param>
        /// <returns>ServiceResponse with pokemon data</returns>
        public async Task<ServiceResponse<Pokemon>> GetPokemonFromPokeApi(string name) {
            _logger.LogDebug($"{DateTime.UtcNow.ToLongTimeString()} - PokemonService - GetPokemonFromPokeApi - Start");
            ServiceResponse<Pokemon> serviceResponse = new ServiceResponse<Pokemon>();
            try {

                var request = new HttpRequestMessage(HttpMethod.Get, $"pokemon-species/{HttpUtility.UrlEncode(name)}");

                var client = _clientFactory.CreateClient(ClientType.PokeApi);

                var response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode) {
                    serviceResponse.Data = await PokeApiDeserialize(response);
                } 

            } catch (Exception ex) {
                _logger.LogError($"{DateTime.UtcNow.ToLongTimeString()} - PokemonService - GetPokemonFromPokeApi exception with message: {ex.Message}");
                serviceResponse.Success = false;
                serviceResponse.Message = ex.Message;
            }
            _logger.LogDebug($"{DateTime.UtcNow.ToLongTimeString()} - PokemonService - GetPokemonFromPokeApi - End");
            return serviceResponse;
        }

        /// <summary>
        /// This method calls the shakespeare endpoint in order to translate the pokemon description
        /// </summary>
        /// <param name="pokemon">It's the pokemon object</param>
        /// <returns>ServiceResponse with pokemon data. The pokemon description is translated</returns>
        public async Task<ServiceResponse<Pokemon>> GetShakespeareanDescription(Pokemon pokemon) {
            _logger.LogDebug($"{DateTime.UtcNow.ToLongTimeString()} - PokemonService - GetShakespeareanDescription - Start");
            ServiceResponse<Pokemon> serviceResponse = new ServiceResponse<Pokemon>();
            try {

                serviceResponse.Data = pokemon;

                var request = new HttpRequestMessage(HttpMethod.Post, "translate/shakespeare.json");

                request.Content = new StringContent(
                                        JsonSerializer.Serialize(new { text = pokemon.Description }),
                                        Encoding.UTF8,
                                        "application/json");

                var client = _clientFactory.CreateClient(ClientType.Shakespeare);

                var response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode) {
                    serviceResponse.Data.Description = await ShakespeareDeserialize(response);
                } else {
                    serviceResponse.Success = false;
                    serviceResponse.Message = "The pokemon description has not been translated by Shakespeare!";
                    _logger.LogError($"{DateTime.UtcNow.ToLongTimeString()} - PokemonService - GetShakespeareanDescription with error: {serviceResponse.Message}");
                }

            } catch (Exception ex) {
                _logger.LogError($"{DateTime.UtcNow.ToLongTimeString()} - PokemonService - GetShakespeareanDescription exception with message: {ex.Message}");
                serviceResponse.Success = false;
                serviceResponse.Message = ex.Message;
            }
            _logger.LogDebug($"{DateTime.UtcNow.ToLongTimeString()} - PokemonService- GetShakespeareanDescription - End");
            return serviceResponse;
        }

        /// <summary>
        /// This method deserialize the PokeApi response in order to create the pokemon object
        /// </summary>
        /// <param name="response">Response message returned by the http request</param>
        /// <returns>Pokemon object</returns>
        private async Task<Pokemon> PokeApiDeserialize(HttpResponseMessage response) {
            _logger.LogDebug($"{DateTime.UtcNow.ToLongTimeString()} - PokemonService - GetPokemonFromPokeApi - PokeApiDeserialize - Start");

            Pokemon pokemon = null;

            using var responseStream = await response.Content.ReadAsStreamAsync();
            var jsonPokemon = await JsonSerializer.DeserializeAsync<JsonPokemon>(responseStream);

            if ((!string.IsNullOrEmpty(jsonPokemon.Name)) && (jsonPokemon.FlavorTextEntries != null || jsonPokemon.FlavorTextEntries.Length > 0)) {

                var flavorTextEntry = jsonPokemon.FlavorTextEntries.Where(element => !string.IsNullOrEmpty(element.FlavorText) && element.Language.Name == "en").FirstOrDefault();

                pokemon = new Pokemon {
                    Name = jsonPokemon.Name,
                    Description = flavorTextEntry.FlavorText.Replace("\n", " ").Replace("\f", " ")
                };

            }
            _logger.LogDebug($"{DateTime.UtcNow.ToLongTimeString()} - PokemonService - GetPokemonFromPokeApi - PokeApiDeserialize - End");
            return pokemon;
        }

        /// <summary>
        /// This method deserialize the Shakespeare response in order to return the translated pokemon description
        /// </summary>
        /// <param name="response">Response message returned by the http request</param>
        /// <returns>Pokemon description translated</returns>
        private async Task<string> ShakespeareDeserialize(HttpResponseMessage response) {
            _logger.LogDebug($"{DateTime.UtcNow.ToLongTimeString()} - PokemonService - GetShakespeareanDescription - ShakespeareDeserialize - Start");

            string descriptionTranslated = "";

            using var responseStream = await response.Content.ReadAsStreamAsync();
            var jsonShakespeare = await JsonSerializer.DeserializeAsync<JsonShakespeare>(responseStream);

            if (jsonShakespeare.Contents != null && !string.IsNullOrEmpty(jsonShakespeare.Contents.Translated)) {
                descriptionTranslated = jsonShakespeare.Contents.Translated;
            }
            
            _logger.LogDebug($"{DateTime.UtcNow.ToLongTimeString()} - PokemonService - GetShakespeareanDescription - ShakespeareDeserialize - End");
            return descriptionTranslated;
        }

    }
}