﻿using System.Threading.Tasks;
using PokemonApi.Models;

namespace PokemonApi.Services.Interfaces {

    /// <summary>
    /// Refer to PokemonService implementation
    /// </summary>
    public interface IPokemonService {

        Task<ServiceResponse<Pokemon>> GetPokemonFromPokeApi(string name);

        Task<ServiceResponse<Pokemon>> GetShakespeareanDescription(Pokemon pokemon);

    }
}