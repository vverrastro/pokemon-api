using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using PokemonApi.Common;
using PokemonApi.Services.Implementations;
using PokemonApi.Services.Interfaces;

namespace PokemonApi {

    public class Startup {

        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {

            // Configuring Swagger service
            services.AddSwaggerGen(options => {
                options.SwaggerDoc("v1", new OpenApiInfo {
                    Version = "v1",
                    Title = "Pokemon Api",
                    Description = "Pokemon Api with ASP.NET Core",
                    Contact = new OpenApiContact {
                        Name = "Vito Verrastro",
                        Email = "verrastro.vito90@gmail.com",
                        Url = new Uri("https://www.linkedin.com/in/vito-verrastro/"),
                    },
                    License = new OpenApiLicense {
                        Name = "MIT License",
                        Url = new Uri("https://gitlab.com/vverrastro/pokemon-api/-/blob/master/LICENSE"),
                    }
                });
                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                options.IncludeXmlComments(xmlPath);
            });

            services.AddControllers().AddJsonOptions(options => {
                options.JsonSerializerOptions.IgnoreNullValues = true;
            }); ;

            // Services registration
            services.AddScoped<IPokemonService, PokemonService>();

            // Clients registration
            services.AddHttpClient(ClientType.PokeApi, c =>
            {
                c.BaseAddress = new Uri("https://pokeapi.co/api/v2/");
                c.DefaultRequestHeaders.Add("Accept", "application/json");
                c.DefaultRequestHeaders.Add("User-Agent", "HttpClientFactory-Sample");
            });
            services.AddHttpClient(ClientType.Shakespeare, c => {
                c.BaseAddress = new Uri("https://api.funtranslations.com/");
                c.DefaultRequestHeaders.Add("Accept", "application/json");
                c.DefaultRequestHeaders.Add("User-Agent", "HttpClientFactory-Sample");
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(options => {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "PokemonApi V1");
                options.RoutePrefix = string.Empty;
                options.DefaultModelsExpandDepth(-1);
            });

            app.UseRouting();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });
        }
    }
}