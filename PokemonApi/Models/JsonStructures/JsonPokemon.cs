﻿using System;
using System.Text.Json.Serialization;

namespace PokemonApi.Models.JsonStructures {

    /// <summary>
    /// Class used to parse the json returned by PokeApi endpoint
    /// </summary>
    public partial class JsonPokemon {

        [JsonPropertyName("base_happiness")]
        public long BaseHappiness { get; set; }

        [JsonPropertyName("capture_rate")]
        public long CaptureRate { get; set; }

        [JsonPropertyName("color")]
        public DynamicNameUrl Color { get; set; }

        [JsonPropertyName("egg_groups")]
        public DynamicNameUrl[] EggGroups { get; set; }

        [JsonPropertyName("evolution_chain")]
        public EvolutionChain EvolutionChain { get; set; }

        [JsonPropertyName("evolves_from_species")]
        public object EvolvesFromSpecies { get; set; }

        [JsonPropertyName("flavor_text_entries")]
        public FlavorTextEntry[] FlavorTextEntries { get; set; }

        [JsonPropertyName("form_descriptions")]
        public object[] FormDescriptions { get; set; }

        [JsonPropertyName("forms_switchable")]
        public bool FormsSwitchable { get; set; }

        [JsonPropertyName("gender_rate")]
        public long GenderRate { get; set; }

        [JsonPropertyName("genera")]
        public Genus[] Genera { get; set; }

        [JsonPropertyName("generation")]
        public DynamicNameUrl Generation { get; set; }

        [JsonPropertyName("growth_rate")]
        public DynamicNameUrl GrowthRate { get; set; }

        [JsonPropertyName("habitat")]
        public DynamicNameUrl Habitat { get; set; }

        [JsonPropertyName("has_gender_differences")]
        public bool HasGenderDifferences { get; set; }

        [JsonPropertyName("hatch_counter")]
        public long HatchCounter { get; set; }

        [JsonPropertyName("id")]
        public long Id { get; set; }

        [JsonPropertyName("is_baby")]
        public bool IsBaby { get; set; }

        [JsonPropertyName("is_legendary")]
        public bool IsLegendary { get; set; }

        [JsonPropertyName("is_mythical")]
        public bool IsMythical { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("names")]
        public Name[] Names { get; set; }

        [JsonPropertyName("order")]
        public long Order { get; set; }

        [JsonPropertyName("pal_park_encounters")]
        public PalParkEncounter[] PalParkEncounters { get; set; }

        [JsonPropertyName("pokedex_numbers")]
        public PokedexNumber[] PokedexNumbers { get; set; }

        [JsonPropertyName("shape")]
        public DynamicNameUrl Shape { get; set; }

        [JsonPropertyName("varieties")]
        public Variety[] Varieties { get; set; }
    }

    public partial class DynamicNameUrl {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("url")]
        public Uri Url { get; set; }
    }

    public partial class EvolutionChain {
        [JsonPropertyName("url")]
        public Uri Url { get; set; }
    }

    public partial class FlavorTextEntry {
        [JsonPropertyName("flavor_text")]
        public string FlavorText { get; set; }

        [JsonPropertyName("language")]
        public DynamicNameUrl Language { get; set; }

        [JsonPropertyName("version")]
        public DynamicNameUrl Version { get; set; }
    }

    public partial class Genus {
        [JsonPropertyName("genus")]
        public string GenusGenus { get; set; }

        [JsonPropertyName("language")]
        public DynamicNameUrl Language { get; set; }
    }

    public partial class Name {
        [JsonPropertyName("language")]
        public DynamicNameUrl Language { get; set; }

        [JsonPropertyName("name")]
        public string NameName { get; set; }
    }

    public partial class PalParkEncounter {
        [JsonPropertyName("area")]
        public DynamicNameUrl Area { get; set; }

        [JsonPropertyName("base_score")]
        public long BaseScore { get; set; }

        [JsonPropertyName("rate")]
        public long Rate { get; set; }
    }

    public partial class PokedexNumber {
        [JsonPropertyName("entry_number")]
        public long EntryNumber { get; set; }

        [JsonPropertyName("pokedex")]
        public DynamicNameUrl Pokedex { get; set; }
    }

    public partial class Variety {
        [JsonPropertyName("is_default")]
        public bool IsDefault { get; set; }

        [JsonPropertyName("pokemon")]
        public DynamicNameUrl Pokemon { get; set; }
    }

}