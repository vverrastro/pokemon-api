﻿using System.Text.Json.Serialization;

namespace PokemonApi.Models.JsonStructures {

    /// <summary>
    /// Class used to parse the json returned by Shakespeare endpoint
    /// </summary>
    public partial class JsonShakespeare {

        [JsonPropertyName("success")]
        public Success Success { get; set; }

        [JsonPropertyName("contents")]
        public Contents Contents { get; set; }

    }

    public partial class Contents {

        [JsonPropertyName("translated")]
        public string Translated { get; set; }

        [JsonPropertyName("text")]
        public string Text { get; set; }

        [JsonPropertyName("translation")]
        public string Translation { get; set; }

    }

    public partial class Success {

        [JsonPropertyName("total")]
        public long Total { get; set; }

    }

}
