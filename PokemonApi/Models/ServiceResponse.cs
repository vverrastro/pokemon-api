﻿namespace PokemonApi.Models {

    /// <summary>
    /// This class represents the object returned by pokemonService
    /// </summary>
    /// <typeparam name="T">Generic type, in the project is Pokemon object</typeparam>
    public class ServiceResponse<T> {

        public string Message { get; set; } = null;

        public bool Success { get; set; } = true;

        public T Data { get; set; }

    }
}