﻿namespace PokemonApi.Models {
    /// <summary>
    /// This class represent pokemon result
    /// </summary>
    public class Pokemon {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}