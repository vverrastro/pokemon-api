﻿FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY PokemonApi/*.csproj ./PokemonApi/
COPY PokemonApi/*.sln ./PokemonApi/
COPY PokemonApi.Test/*.csproj ./PokemonApi.Test/

RUN dotnet restore ./PokemonApi/PokemonApi.sln

# Copy everything else and build
COPY ./PokemonApi/ ./PokemonApi/
COPY ./PokemonApi.Test/ ./PokemonApi.Test/
RUN dotnet build ./PokemonApi/PokemonApi.sln

FROM build AS publish
# set the working directory to be the web api project
WORKDIR /app/PokemonApi

# publish the web api project to a directory called out
RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS runtime
WORKDIR /app

COPY --from=publish /app/PokemonApi/out ./

# expose port 80 as our application will be listening on this port
EXPOSE 80

ENTRYPOINT ["dotnet", "PokemonApi.dll"]