﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.Protected;
using PokemonApi.Models;
using PokemonApi.Services.Implementations;
using Xunit;
using Microsoft.AspNetCore.Mvc.Testing;
using System.IO;

namespace PokemonApi.Test {

    /// <summary>
    /// This class includes the unit tests
    /// </summary>
    public class PokemonApiUnitTest : IClassFixture<WebApplicationFactory<Startup>> {

        private readonly ILogger<PokemonService> _logger;


        private const string PokeapiBaseAddress = "https://pokeapi.co/api/v2/";
        private const string ShakespeareBaseAddress = "https://api.funtranslations.com/";
        private const string JsonPokeapiResponse = "pokeapiResponse.json";
        private const string JsonShakespeareResponse = "shakespeareResponse.json";

        public PokemonApiUnitTest() {
            var serviceProvider = new ServiceCollection().AddLogging().BuildServiceProvider();
            _logger = serviceProvider.GetService<ILoggerFactory>().CreateLogger<PokemonService>();
        }

        [Fact(DisplayName = "Is valid pokemon")]
        public async void IsValidPokemon() {
            PokemonService pokemonService = new PokemonService(_logger, GetMockClientFactory(PokeapiBaseAddress, JsonPokeapiResponse));
            ServiceResponse<Pokemon> response = await pokemonService.GetPokemonFromPokeApi("bulbasaur");
            Assert.NotNull(response.Data);
        }

        [Fact(DisplayName = "Is valid pokemon name")]
        public async void IsValidPokemonName() {
            PokemonService pokemonService = new PokemonService(_logger, GetMockClientFactory(PokeapiBaseAddress, JsonPokeapiResponse));
            ServiceResponse<Pokemon> response = await pokemonService.GetPokemonFromPokeApi("bulbasaur");
            Assert.NotNull(response.Data.Name);
        }

        [Fact(DisplayName = "Is valid pokemon description")]
        public async void IsValidPokemonDescription() {
            PokemonService pokemonService = new PokemonService(_logger, GetMockClientFactory(PokeapiBaseAddress, JsonPokeapiResponse));
            ServiceResponse<Pokemon> response = await pokemonService.GetPokemonFromPokeApi("bulbasaur");
            Assert.NotNull(response.Data.Description);
        }

        [Fact(DisplayName = "Is valid pokemon returned by pokeapi")]
        public async void IsValidPokemonReturnedByPokeApi() {
            PokemonService pokemonService = new PokemonService(_logger, GetMockClientFactory(PokeapiBaseAddress, JsonPokeapiResponse));
            string name = "bulbasaur";
            ServiceResponse<Pokemon> response = await pokemonService.GetPokemonFromPokeApi(name);
            Assert.Equal(name, response.Data.Name);
        }

        [Fact(DisplayName = "Is valid schakespearean translation")]
        public async void IsValidShakespeareanTranslation() {
            PokemonService pokemonService = new PokemonService(_logger, GetMockClientFactory(ShakespeareBaseAddress, JsonShakespeareResponse));
            Pokemon pokemon = new Pokemon();
            pokemon.Name = "charizard";
            pokemon.Description = "Spits fire that is hot enough to melt boulders. Known to cause forest fires unintentionally.";
            ServiceResponse<Pokemon> response = await pokemonService.GetShakespeareanDescription(pokemon);
            Assert.Equal("Spits fire yond is hot enow to melt boulders. Known to cause forest fires unintentionally.", response.Data.Description);
        }

        [Fact(DisplayName = "Is valid schakespearean translation completed")]
        public async void IsShakespeareanTranslationCompleted() {
            PokemonService pokemonService = new PokemonService(_logger, GetMockClientFactory(ShakespeareBaseAddress, JsonShakespeareResponse));
            Pokemon pokemon = new Pokemon();
            pokemon.Name = "charizard";
            pokemon.Description = "Spits fire that is hot enough to melt boulders. Known to cause forest fires unintentionally.";
            ServiceResponse<Pokemon> response = await pokemonService.GetShakespeareanDescription(pokemon);
            Assert.NotEqual("The pokemon description has not been translated by Shakespeare!", response.Message);
        }

        /// <summary>
        /// This method creates a mock client factory in order to test pokemonService methods
        /// </summary>
        /// <param name="baseAddress">Base address for mock client factory</param>
        /// <param name="mockJsonResponse">Json file name with mock response</param>
        /// <returns>Mock client factory</returns>
        private IHttpClientFactory GetMockClientFactory(string baseAddress, string mockJsonResponse) {
            var httpClientFactory = new Mock<IHttpClientFactory>();
            var mockHttpMessageHandler = new Mock<HttpMessageHandler>();

            var json = File.ReadAllText(mockJsonResponse);

            mockHttpMessageHandler.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(json)
                });

            var client = new HttpClient(mockHttpMessageHandler.Object);
            client.BaseAddress = new Uri(baseAddress);
            httpClientFactory.Setup(_ => _.CreateClient(It.IsAny<string>())).Returns(client);
            return httpClientFactory.Object;
        }

    }
}
