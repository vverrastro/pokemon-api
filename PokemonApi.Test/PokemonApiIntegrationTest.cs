using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Xunit;

namespace PokemonApi.IntegrationTest {

    /// <summary>
    /// This class is an integration test for Pokemon API
    /// </summary>
    public class PokemonApiIntegrationTest {

        private readonly TestServer _server;
        private readonly HttpClient _client;

        public PokemonApiIntegrationTest() {
            _server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            _client = _server.CreateClient();
        }

        /// <summary>
        /// Get a pokemon
        /// </summary>
        /// <param name="method">It's the verb passed to the function</param>
        /// <param name="name"> It's the pokemon name</param>
        /// <returns></returns>
        [Theory(DisplayName = "Get a pokemon")]
        [InlineData("GET", "bulbasaur")]
        public async Task GetPokemonAsync(string method, string name) {
            // arrange
            var request = new HttpRequestMessage(new HttpMethod(method), $"/pokemon/{name}");
            // act
            var response = await _client.SendAsync(request);
            // assert
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        /// <summary>
        /// Get a pokemon with number
        /// </summary>
        /// <param name="method">It's the verb passed to the function</param>
        /// <param name="name"> It's the pokemon name</param>
        /// <returns></returns>
        [Theory(DisplayName = "Get a pokemon with number")]
        [InlineData("GET", "2")]
        public async Task GetPokemonWithNumberAsync(string method, string name) {
            // arrange
            var request = new HttpRequestMessage(new HttpMethod(method), $"/pokemon/{name}");
            // act
            var response = await _client.SendAsync(request);
            // assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        /// <summary>
        /// Get a not found status code because of a wrong pokemon name
        /// </summary>
        /// <param name="method">It's the verb passed to the function</param>
        /// <param name="name"> It's a casual string</param>
        /// <returns></returns>
        [Theory(DisplayName = "Get a not found because of a wrong pokemon name")]
        [InlineData("GET", "supermario")]
        public async Task GetPokemonNotFoundAsync(string method, string name) {
            // arrange
            var request = new HttpRequestMessage(new HttpMethod(method), $"/pokemon/{name}");
            // act
            var response = await _client.SendAsync(request);
            // assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

    }

}