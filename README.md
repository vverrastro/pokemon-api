# Pokemon API

It's an API created using .NET Core, given a Pokemon name, returns its Shakespearean description and its name.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need

- **dotnet 3.1** _(check from the terminal using the command **dotnet --version**)_
- **docker** _(to test the Dockerfile)_


### Installing

How to install

1. Clone the repository in your local (git clone https://gitlab.com/vverrastro/pokemon-api.git)
2. Installing **dotnet core SDK and Runtime** from the following [page](https://dotnet.microsoft.com/download/dotnet-core)
3. Installing **docker desktop** from the following [page](https://www.docker.com/get-started__)

## Running the tests

- Test (from base directory) - **dotnet test ./PokemonApi.Test/**

## Running the project

1. Run (from base directory) - **dotnet run --project ./PokemonApi/**
2. **Browser**: `http://localhost:5000/pokemon/{name}`
3. **Swagger**: `http://localhost:5000/index.html `
4. **Postman**: GET `http://localhost:5000/pokemon/{name}`

The JSON returned by the endpoint has the following structure

```Json
{
  "message": "string",
  "success": true,
  "data": {
    "name": "string",
    "description": "string"
  }
}
```

### Dockerfile

In the project you will find the [Dockerfile](https://gitlab.com/vverrastro/pokemon-api/-/blob/master/Dockerfile)

To test from the base directory:

```
docker build -t pokemonapi:latest .
docker run --rm -it -p 5000:80 pokemonapi:latest
```

## Resources

Following APIs have been used for the project.
- Shakespeare translation https://funtranslations.com/api/shakespeare (For free plan used in the project there are 60 API calls a day with distribution of 5 calls an hour)
- PokeApi https://pokeapi.co/

For the pokemon description I used the param _flavor_text_entries_ returned by the following endpoint:
- /pokemon-species/ - Ref [PokeAPI Issue](https://github.com/PokeAPI/pokeapi/issues/107)

## Roadmap

- Update the format response to be more clear **(Completed)**
- Configure the project for the deploy in production
- Configure CI/CD for tests and deploy

## Authors

* **Vito Verrastro** - verrastro.vito90@gmail.com

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
